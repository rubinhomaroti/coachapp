package br.com.coachapp.testes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import br.com.coachapp.dao.CoachDAO;
import br.com.coachapp.dao.CoacheeDAO;
import br.com.coachapp.dao.JournalDAO;
import br.com.coachapp.dao.SessionDAO;
import br.com.coachapp.dao.SessionPackageDAO;
import br.com.coachapp.dao.SpecialtyDAO;
import br.com.coachapp.entities.Coach;
import br.com.coachapp.entities.Coachee;
import br.com.coachapp.entities.Journal;
import br.com.coachapp.entities.Session;
import br.com.coachapp.entities.SessionPackage;
import br.com.coachapp.entities.Specialty;

public class TesteJPA {

	public static void main(String[] args) {
		EntityManager em = null;
		try {
			em = Persistence.createEntityManagerFactory("coachapp").createEntityManager();

			// Cadastro de especialidades
			Specialty specialty1 = createSpecialty(em, "Relacionamento", null, null);
			Specialty specialty2 = createSpecialty(em, "Financeiro", null, null);
			Specialty specialty3 = createSpecialty(em, "Carreira", null, null);
			
			ArrayList<Specialty> specialtyList1 = new ArrayList<Specialty>();
			specialtyList1.add(specialty1);
			
			ArrayList<Specialty> specialtyList2 = new ArrayList<Specialty>();
			specialtyList2.add(specialty2);
			specialtyList2.add(specialty3);
			//

			// Cadastro de Coachee
			Coachee coachee1 = createCoachee(em, 
											"Gabriel", 
											new GregorianCalendar(1997, 05, 9), 
											"M", 
											"12345678910",
											912345678D, 
											"gfragata97@gmail.com", 
											"games123@", 
											null, 
											null, 
											null);
			
			Coachee coachee2 = createCoachee(em, 
											"Jessica", 
											new GregorianCalendar(1997, 01, 01), 
											"F", 
											"12345678911",
											99998888D, 
											"jessica@gmail.com", 
											"jeje123@", 
											null, 
											null, 
											null);
			
			Coachee coachee3 = createCoachee(em, 
											"Marcielle", 
											new GregorianCalendar(1998, 04, 20), 
											"F", 
											"12345678912",
											977776666D, 
											"marcielle@gmail.com", 
											"marci@", 
											null, 
											null, 
											null);
			
			ArrayList<Coachee> coacheesList = new ArrayList<Coachee>();
			coacheesList.add(coachee1);
			coacheesList.add(coachee2);
			coacheesList.add(coachee3);
			//
			
			// Cadastro de coach
			Coach coach1 = createCoach(em, 
									  "Fernandinha", 
									  new GregorianCalendar(1985, 05, 22), 
									  "F", 
									  "22223333355", 
									  922223333D,
									  "fefe@gmail.com", 
									  "fefe#", 
									  null, 
									  0.1F, 
									  specialtyList1, 
									  coacheesList);
			
			Coach coach2 = createCoach(em, 
									  "Rubens", 
									  new GregorianCalendar(1997, 01, 01), 
									  "M", 
									  "46471212836", 
									  991818698D,
									  "rubinho.maroti@gmail.com", 
									  "surf1234", 
									  null, 
									  0.1F, 
									  specialtyList2, 
									  coacheesList);
			//
			
			// Cadastro de pacote de sessões
			SessionPackage package1 = createPackage(em, 5, 25000D, null, coach1);
			SessionPackage package2 = createPackage(em, 5, 18000D, null, coach2);
			//
			
			// Cadastro de sessões
			Session session1 = createSession(em, 
											 coach1, 
											 coachee1, 
											 "Descrição teste", 
											 true, 
											 "https://meet.google.com/abcdef", 
											 null,
											 new GregorianCalendar(2021, 05, 24, 19, 45), 
											 new GregorianCalendar(2021, 05, 24, 21, 30), 
											 1, 
											 package1,
											 specialty1, 
											 "Jessica tem sorriso bonito");
			
			Session session2 = createSession(em, 
											 coach2, 
											 coachee2, 
											 "Descrição teste 2", 
											 true, 
											 "https://meet.google.com/ghzcjs", 
											 null,
											 new GregorianCalendar(2021, 05, 25, 10, 00), 
											 new GregorianCalendar(2021, 05, 25, 11, 30), 
											 1, 
											 package2,
											 specialty2, 
											 "Rubinho e o surf");
			
			ArrayList<Session> sessionsCoach1 = new ArrayList<Session>();
			sessionsCoach1.add(session1);
			
			ArrayList<Session> sessionsCoach2 = new ArrayList<Session>();
			sessionsCoach2.add(session2);
			//
			
			// Cadastro de diário
			Journal journal1 = createJournal(em, coach1, coachee1, null, null, null, sessionsCoach1);
			Journal journal2 = createJournal(em, coach1, coachee2, null, null, null, sessionsCoach2);
			//

			listAll(em);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
			System.out.println("Teste finalizado");
			System.exit(0);
		}
	}

	private static void listAll(EntityManager em) {
		CoachDAO coachDao = new CoachDAO(em);
		for (Coach coach : coachDao.all()) {
			System.out.println("Coach: " + coach);
			System.out.println("Taxa de cancelamento: " + coach.getCancellationFee() * 100 + "%");
			
			System.out.println("Especialidades atendidas:");
			for (Specialty specialty : coach.getSpecialties()) {
				System.out.println("- " + specialty);
			}
			
			System.out.println("Coachees:");
			for (Coachee coachee : coach.getCoachees()) {
				System.out.println("- " + coachee);
			}
			
			System.out.println("-----------------------------------------");
		}
		
		SessionDAO sessionDAO = new SessionDAO(em);
		for (Session session : sessionDAO.all()) {
			System.out.println(session);
			System.out.println("Sessão " + session.getSessionNumber() + "/" + session.getSessionPackage().getTotalSessions());
			System.out.println("Início: " + session.getScheduledDateTimeStart().getTime());
			System.out.println("Fim: " + session.getScheduledDateTimeEnd().getTime());
			System.out.println("Especialidade: " + session.getSpecialty());
			System.out.println("Preço individual: " + session.getSessionPackage().getIndividualSessionPrice());
			
			System.out.println("-----------------------------------------");
		}
	}

	private static Coach createCoach(EntityManager em, 
									 String name, 
									 Calendar birthDate, 
									 String sex, 
									 String cpf,
									 Double contactNumber, 
									 String email, 
									 String password, 
									 String cnpj, 
									 Float cancellationFee, 
									 Collection<Specialty> specialties,
									 Collection<Coachee> coachees) {
		Coach c = new Coach();
		c.setName(name);
		c.setBirthDate(birthDate);
		c.setSex(sex);
		c.setCpf(cpf);
		c.setContactNumber(contactNumber);
		c.setEmail(email);
		c.setPassword(password);
		c.setCnpj(cnpj);
		c.setCancellationFee(cancellationFee);
		c.setSpecialties(specialties);
		c.setCoachees(coachees);
		CoachDAO dao = new CoachDAO(em);
		dao.save(c);
		return c;
	}

	private static Coachee createCoachee(EntityManager em, 
										 String name, 
										 Calendar birthDate, 
										 String sex, 
										 String cpf,
										 Double contactNumber, 
										 String email, 
										 String password, 
										 Collection<Coach> coachs,
										 Collection<Journal> journal, 
										 Collection<Session> session) {
		Coachee c = new Coachee();
		c.setName(name);
		c.setBirthDate(birthDate);
		c.setSex(sex);
		c.setCpf(cpf);
		c.setContactNumber(contactNumber);
		c.setEmail(email);
		c.setPassword(password);
		c.setCoachs(coachs);
		c.setJournals(journal);
		c.setSessions(session);
		CoacheeDAO dao = new CoacheeDAO(em);
		dao.save(c);
		return c;
	}

	private static Journal createJournal(EntityManager em, 
									     Coach coach, 
									     Coachee coachee, 
									     String learnings,
									     String nextSteps, 
									     String notes, 
									     Collection<Session> sessions) {
		Journal c = new Journal();
		c.setCoach(coach);
		c.setCoachee(coachee);
		c.setLearnings(learnings);
		c.setNextSteps(nextSteps);
		c.setNotes(notes);
		c.setSessions(sessions);
		JournalDAO dao = new JournalDAO(em);
		dao.save(c);
		return c;
	}
	
	private static SessionPackage createPackage(EntityManager em,
												Integer totalSessions,
												Double price,
												Collection<Session> sessions,
												Coach coach) {
		SessionPackage sessionPackage = new SessionPackage();
		sessionPackage.setTotalSessions(totalSessions);
		sessionPackage.setPrice(price);
		sessionPackage.setSessions(sessions);
		sessionPackage.setCoach(coach);
		SessionPackageDAO packageDAO = new SessionPackageDAO(em);
		packageDAO.save(sessionPackage);
		return sessionPackage;
	}

	private static Session createSession(EntityManager em, 
										 Coach coach, 
										 Coachee coachee, 
										 String description,
								 		 Boolean hasCancellationFee, 
								 		 String inviteUrl, 
								 		 Journal journal, 
								 		 Calendar scheduledDateTimeStart,
								 		 Calendar scheduledDateTimeEnd,
							 		 	 Integer sessionNumber, 
							 		 	 SessionPackage sessionPackage,
							 		 	 Specialty specialty, 
							 		 	 String title) {
		Session s = new Session();
		s.setCoach(coach);
		s.setCoachee(coachee);
		s.setDescription(description);
		s.setHasCancellationFee(hasCancellationFee);
		s.setInviteUrl(inviteUrl);
		s.setJournal(journal);
		s.setScheduledDateTimeStart(scheduledDateTimeStart);
		s.setScheduledDateTimeEnd(scheduledDateTimeEnd);
		s.setSessionNumber(sessionNumber);
		s.setSessionPackage(sessionPackage);
		s.setSpecialty(specialty);
		s.setTitle(title);
		SessionDAO dao = new SessionDAO(em);
		dao.save(s);
		return s;
	}

	private static Specialty createSpecialty(EntityManager em, 
											 String description,
											 Collection<Coach> coach, 
											 Collection<Session> session) {
		Specialty s = new Specialty();
		s.setCoachs(coach);
		s.setDescription(description);
		s.setSessions(session);
		SpecialtyDAO dao = new SpecialtyDAO(em);
		dao.save(s);
		return s;
	}

}