package br.com.coachapp.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tb_coach")
public class Coach extends User implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public Coach() {
		super();
	}

	@Column(length = 16, nullable = true)
	private String cnpj;
	
	@Column(name = "cancellation_fee", nullable = false)
	private Float cancellationFee;
	
	@OneToMany(mappedBy = "coach", cascade = CascadeType.PERSIST)
	private Collection<Session> sessions;
	
	@OneToMany(mappedBy = "coach", cascade = CascadeType.PERSIST)
	private Collection<Journal> journals;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "tb_coach_specialty", catalog = "coachapp", joinColumns = {
			@JoinColumn(name = "coach_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "specialty_id", nullable = false, updatable = false) })
	private Collection<Specialty> specialties;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "tb_coach_coachee", catalog = "coachapp", joinColumns = {
			@JoinColumn(name = "coach_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "coachee_id", nullable = false, updatable = false) })
	private Collection<Coachee> coachees;
	
	@OneToMany(mappedBy = "coach", cascade = CascadeType.PERSIST)
	private Collection<SessionPackage> sessionPackages;
	
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Float getCancellationFee() {
		return cancellationFee;
	}

	public void setCancellationFee(Float cancellationFee) {
		this.cancellationFee = cancellationFee;
	}

	public Collection<Session> getSessions() {
		return sessions;
	}

	public void setSessions(Collection<Session> sessions) {
		this.sessions = sessions;
	}

	public Collection<Journal> getJournals() {
		return journals;
	}

	public void setJournals(Collection<Journal> journals) {
		this.journals = journals;
	}

	public Collection<Specialty> getSpecialties() {
		return specialties;
	}

	public void setSpecialties(Collection<Specialty> specialties) {
		this.specialties = specialties;
	}

	public Collection<Coachee> getCoachees() {
		return coachees;
	}

	public void setCoachees(Collection<Coachee> coachees) {
		this.coachees = coachees;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
