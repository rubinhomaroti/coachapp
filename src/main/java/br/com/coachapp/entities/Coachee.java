package br.com.coachapp.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tb_coachee")
public class Coachee extends User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Coachee() {
		super();
	}
	
	@OneToMany(mappedBy = "coachee", cascade = CascadeType.PERSIST)
	private Collection<Session> sessions;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "tb_coach_coachee", catalog = "coachapp", joinColumns = {
			@JoinColumn(name = "coach_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "coachee_id", nullable = false, updatable = false) })
	private Collection<Coach> coachs;
	
	@OneToMany(mappedBy = "coachee", cascade = CascadeType.PERSIST)
	private Collection<Journal> journals;

	public Collection<Session> getSessions() {
		return sessions;
	}

	public void setSessions(Collection<Session> sessions) {
		this.sessions = sessions;
	}

	public Collection<Coach> getCoachs() {
		return coachs;
	}

	public void setCoachs(Collection<Coach> coachs) {
		this.coachs = coachs;
	}

	public Collection<Journal> getJournals() {
		return journals;
	}

	public void setJournals(Collection<Journal> journals) {
		this.journals = journals;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
