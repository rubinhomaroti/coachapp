package br.com.coachapp.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tb_session")
public class Session implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public Session() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "scheduled_date_time_start", nullable = false)
	private Calendar scheduledDateTimeStart;
	
	@Column(name = "scheduled_date_time_end", nullable = false)
	private Calendar scheduledDateTimeEnd;
	
	@Column(name = "session_number", nullable = false)
	private Integer sessionNumber;
	
	@Column(nullable = false, length = 32)
	private String title;
	
	@Column(nullable = true, length = 64)
	private String description;
	
	@Column(name = "invite_url", nullable = true, length = 255)
	private String inviteUrl;
	
	@Column(name = "has_cancellation_fee", nullable = false)
	private Boolean hasCancellationFee;
	
	@ManyToOne(optional = false)
	private Coach coach;
	
	@ManyToOne(optional = false)
	private Coachee coachee;
	
	@ManyToOne(optional = false)
	private Specialty specialty;
	
	@ManyToOne(optional = true)
	private Journal journal;
	
	@ManyToOne(optional = false)
	private SessionPackage sessionPackage;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Calendar getScheduledDateTimeStart() {
		return scheduledDateTimeStart;
	}

	public void setScheduledDateTimeStart(Calendar scheduledDateTimeStart) {
		this.scheduledDateTimeStart = scheduledDateTimeStart;
	}

	public Calendar getScheduledDateTimeEnd() {
		return scheduledDateTimeEnd;
	}

	public void setScheduledDateTimeEnd(Calendar scheduledDateTimeEnd) {
		this.scheduledDateTimeEnd = scheduledDateTimeEnd;
	}

	public Integer getSessionNumber() {
		return sessionNumber;
	}

	public void setSessionNumber(Integer sessionNumber) {
		this.sessionNumber = sessionNumber;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInviteUrl() {
		return inviteUrl;
	}

	public void setInviteUrl(String inviteUrl) {
		this.inviteUrl = inviteUrl;
	}

	public Boolean getHasCancellationFee() {
		return hasCancellationFee;
	}

	public void setHasCancellationFee(Boolean hasCancellationFee) {
		this.hasCancellationFee = hasCancellationFee;
	}

	public Coach getCoach() {
		return coach;
	}

	public void setCoach(Coach coach) {
		this.coach = coach;
	}

	public Coachee getCoachee() {
		return coachee;
	}

	public void setCoachee(Coachee coachee) {
		this.coachee = coachee;
	}

	public Specialty getSpecialty() {
		return specialty;
	}

	public void setSpecialty(Specialty specialty) {
		this.specialty = specialty;
	}

	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}
	
	public SessionPackage getSessionPackage() {
		return sessionPackage;
	}

	public void setSessionPackage(SessionPackage sessionPackage) {
		this.sessionPackage = sessionPackage;
	}
	
	@Override
	public String toString() {
		return title;
	}
}
