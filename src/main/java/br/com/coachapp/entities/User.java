package br.com.coachapp.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="tb_user")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public User() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Integer id;
	
	@Column(length = 56, nullable = false)
	protected String name;
	
	@Column(nullable = false, name = "birth_date")
	protected Calendar birthDate;
	
	@Column(length = 1, columnDefinition="CHAR", nullable = false)
	protected String sex;
	
	@Column(length = 16, nullable = false)
	protected String cpf;
	
	@Column(name = "contact_number", nullable = true)
	protected Double contactNumber;
	
	@Column(nullable = false, length = 56)
	protected String email;
	
	@Column(nullable = false, length = 56)
	protected String password;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Calendar getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf.replaceAll(".", "").replaceAll("-", "");
	}

	public Double getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Double contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) { 
		this.password = password;
	}
}
