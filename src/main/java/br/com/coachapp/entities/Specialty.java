package br.com.coachapp.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tb_specialty")
public class Specialty implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public Specialty() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false, length = 32)
	private String description;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "tb_coach_specialty", catalog = "coachapp", joinColumns = {
			@JoinColumn(name = "coach_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "specialty_id", nullable = false, updatable = false) })
	private Collection<Coach> coachs;
	
	@OneToMany(mappedBy = "specialty", cascade = CascadeType.PERSIST)
	private Collection<Session> sessions;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Collection<Coach> getCoachs() {
		return coachs;
	}

	public void setCoachs(Collection<Coach> coachs) {
		this.coachs = coachs;
	}

	public Collection<Session> getSessions() {
		return sessions;
	}

	public void setSessions(Collection<Session> sessions) {
		this.sessions = sessions;
	}
	
	@Override
	public String toString() {
		return description;
	}
}
