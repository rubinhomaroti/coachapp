package br.com.coachapp.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tb_journal")
public class Journal implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Journal() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = true, length = 255)
	private String learnings;
	
	@Column(name = "next_steps", nullable = true, length = 255)
	private String nextSteps;
	
	@Column(nullable = true, length = 255)
	private String notes;
	
	@ManyToOne
	private Coach coach;
	
	@ManyToOne
	private Coachee coachee;
	
	@OneToMany(mappedBy = "journal", cascade = CascadeType.PERSIST)
	private Collection<Session> sessions;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLearnings() {
		return learnings;
	}

	public void setLearnings(String learnings) {
		this.learnings = learnings;
	}

	public String getNextSteps() {
		return nextSteps;
	}

	public void setNextSteps(String nextSteps) {
		this.nextSteps = nextSteps;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Coach getCoach() {
		return coach;
	}

	public void setCoach(Coach coach) {
		this.coach = coach;
	}

	public Coachee getCoachee() {
		return coachee;
	}

	public void setCoachee(Coachee coachee) {
		this.coachee = coachee;
	}

	public Collection<Session> getSessions() {
		return sessions;
	}

	public void setSessions(Collection<Session> sessions) {
		this.sessions = sessions;
	}
	
	
}
