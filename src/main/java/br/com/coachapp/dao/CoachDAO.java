package br.com.coachapp.dao;

import javax.persistence.EntityManager;

import br.com.coachapp.entities.Coach;

public class CoachDAO extends GenericDAO<Coach, Integer>{

	public CoachDAO(EntityManager em) {
		super(em);
	}
}