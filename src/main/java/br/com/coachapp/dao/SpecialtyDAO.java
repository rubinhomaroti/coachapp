package br.com.coachapp.dao;

import javax.persistence.EntityManager;

import br.com.coachapp.entities.Specialty;

public class SpecialtyDAO extends GenericDAO<Specialty,Integer> {

	public SpecialtyDAO(EntityManager em) {
		super(em);
	}
}