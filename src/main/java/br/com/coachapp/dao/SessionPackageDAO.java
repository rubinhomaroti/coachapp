package br.com.coachapp.dao;

import javax.persistence.EntityManager;

import br.com.coachapp.entities.SessionPackage;

public class SessionPackageDAO extends GenericDAO<SessionPackage, Integer>{

	public SessionPackageDAO(EntityManager em) {
		super(em);
	}
}
