package br.com.coachapp.dao;

import javax.persistence.EntityManager;

import br.com.coachapp.entities.Session;

public class SessionDAO extends GenericDAO<Session,Integer> {

	public SessionDAO(EntityManager em) {
		super(em);
	}
}