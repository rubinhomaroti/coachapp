package br.com.coachapp.dao;

import javax.persistence.EntityManager;

import br.com.coachapp.entities.Coachee;

public class CoacheeDAO extends GenericDAO<Coachee, Integer> {

	public CoacheeDAO(EntityManager em) {
		super(em);
	}
}