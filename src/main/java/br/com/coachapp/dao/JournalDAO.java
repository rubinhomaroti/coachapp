package br.com.coachapp.dao;

import javax.persistence.EntityManager;

import br.com.coachapp.entities.Journal;

public class JournalDAO extends GenericDAO<Journal, Integer> {

	public JournalDAO(EntityManager em) {
		super(em);
	}
}