package br.com.coachapp.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;

public abstract class GenericDAO<E, K> {

	protected Class<E> entityClass;

	protected EntityManager em;

	@SuppressWarnings("unchecked")
	public GenericDAO(EntityManager em) {
		this.em = em;
		this.entityClass = (Class<E>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	public void save(E entity) {
		this.em.persist(entity);
		commit();
	}

	public E find(K key) throws Exception {
		E entity = this.em.find(entityClass, key);
		if (entity == null)
			throw new Exception("Entity not found");
		return entity;
	}

	public List<E> all() {
		return this.em.createQuery("from " + entityClass.getName(), entityClass).getResultList();
	}

	public List<E> all(int page, int itensPerPage) {
		int first = (page - 1) * itensPerPage;
		return this.em.createQuery("from " + entityClass.getName(), entityClass).setMaxResults(itensPerPage)
				.setFirstResult(first).getResultList();
	}

	public void remove(K key) throws Exception {
		E entity = find(key);
		em.remove(entity);
		commit();
	}

	protected void commit() {
		try {
			em.getTransaction().begin();
			em.getTransaction().commit();
		} catch (Exception e) {
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();

			throw e;
		}
	}
}